//Este ejercicio consistirá en leer el archivo users.json e imprimir en consola el contenido de ese archivo

//Imports de los módulos
const fs = require("fs");
const path = require("path");
const pathFile1 = path.resolve("users.json");
const pathFile2 = path.resolve("hello.txt");

const readFileUsers = () => {
    //Imprimir en consola el arreglo de usuarios
    fs.readFile(pathFile1, "UTF-8", (error, data) => {
        if (error) {
            return console.log("Ups, aqui hay un error: ",error)
        }
        return console.log("ReadFile method success: ",data);
    })
}

const writeHelloWorld = () => {
    //Escribir Hello world! en el archivo hello.txt
    let data = "hello world";
    fs.writeFile(pathFile2, data, (error) => {
        error ? console.log("Ups, aqui hay un error: ",error) : console.log("WriteFile method success: ",data);
    })
}

readFileUsers();
writeHelloWorld();